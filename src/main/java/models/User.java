package models;


import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.persistence.Id;

@Entity
@Table(name = "User", schema="Security")
public class User {

	@Id  
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "UserID", unique = true, nullable = false)
    public UUID userID;
   
    @Column(name = "Nickname", nullable = false)
    public String description;
 
    @Column(name = "Password", nullable = false)
    public String password;
    
    @Column(name = "EMail")
    public String email;
 
    @Version
    public long version;

	public UUID getUserID() {
		return userID;
	}

	public void setUserID(UUID id) {
		this.userID = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
}
