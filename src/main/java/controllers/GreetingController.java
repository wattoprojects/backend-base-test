package controllers;

import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import models.Greeting;

@RestController

public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
    final static Logger log = Logger.getLogger(GreetingController.class);
    
    @RequestMapping("/greeting")
    public  Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
    	log.error("ERROR");
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}